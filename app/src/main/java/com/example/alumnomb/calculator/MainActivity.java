package com.example.alumnomb.calculator;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    EditText eT_nota1, eT_nota2, eT_nota3, eT_porc1, eT_porc2, eT_porc3;
    Button btn_promedio;
    Integer promedio_n;
    TextView tV_promedio_n, textlist;
    ListView list;
    ArrayAdapter<String> adapter;
    ArrayList<String> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        eT_nota1 = (EditText) findViewById(R.id.eT_nota1);
        eT_nota2 = (EditText) findViewById(R.id.eT_nota2);
        eT_nota3 = (EditText) findViewById(R.id.eT_nota3);
        eT_porc1 = (EditText) findViewById(R.id.eT_porc1);
        eT_porc2 = (EditText) findViewById(R.id.eT_porc2);
        eT_porc3 = (EditText) findViewById(R.id.eT_porc3);
        btn_promedio = (Button) findViewById(R.id.btn_promedio);
        tV_promedio_n = (TextView) findViewById(R.id.tV_promedio_n);
        list = (ListView) findViewById(R.id.lV_resultados);
        textlist = (TextView) findViewById(R.id.textlist);
        arrayList = new ArrayList<>();
        adapter = new ArrayAdapter<String>(getApplicationContext(),android.R.layout.simple_spinner_item,arrayList);
        list.setAdapter(adapter);

        btn_promedio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                        double nota1 = Double.parseDouble(eT_nota1.getText().toString());
                        double nota2 = Double.parseDouble(eT_nota2.getText().toString());
                        double nota3 = Double.parseDouble(eT_nota3.getText().toString());
                        double porc1 = Double.parseDouble(eT_porc1.getText().toString());
                        double porc2 = Double.parseDouble(eT_porc2.getText().toString());
                        double porc3 = Double.parseDouble(eT_porc3.getText().toString());
                        if (nota1 >= 10 && nota2 >= 10 && nota3 >= 10 && nota1 <= 70 && nota2 <= 70 && nota3 <= 70 &&
                                porc1 >= 0 && porc2 >= 0 && porc3 >= 0 && porc1 <= 100 && porc2 <= 100 && porc3 <= 100 &&
                                (porc1 + porc2 + porc3 == 100)) {
                            promedio_n = (int) (((nota1 * porc1) + (nota2 * porc2) + (nota3 * porc3)) / 100);

                        if (promedio_n < 40)
                            tV_promedio_n.setTextColor(Color.RED);
                        else
                            tV_promedio_n.setTextColor(Color.BLUE);

                        tV_promedio_n.setText(promedio_n.toString());
                        textlist.setText("Nota1: " + nota1 + ", Nota2: " + nota2 + ", Nota3: " + nota3 + " - Prim: " + promedio_n);

                         arrayList.add(textlist.getText().toString());
                        adapter.notifyDataSetChanged();

                        }
                        else
                            Toast.makeText(MainActivity.this, "Debe cumplir con la escala de notas", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "Debe ingresar todos los valores", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            eT_nota1.setText("");
            eT_nota2.setText("");
            eT_nota3.setText("");
            eT_porc1.setText("");
            eT_porc2.setText("");
            eT_porc3.setText("");
            tV_promedio_n.setText("");
            list.setAdapter(null);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
}
